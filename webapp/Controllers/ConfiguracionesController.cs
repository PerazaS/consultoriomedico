﻿using SmartAdminMvc.DB;
using SmartAdminMvc.Models.Configuraciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartAdminMvc.Controllers
{
    public class ConfiguracionesController : Controller
    {

        #region Departamentos

        // GET: Configuraciones
        public ActionResult Departamentos()
        {
            return View();
        }

        public JsonResult GetDepartamentos()
        {
            using (var db = new ConsultorioEntities())
            {
                var model = db.Departamentos.Select(x => new DepartamentosViewModel
                {
                    idDepartamentos = x.idDepartamento,
                    descripcion = x.descripcion
                }).ToList();

                return Json(new { data = model }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult CrearDepartamentos()
        {
            return PartialView(new DepartamentosViewModel());
        }


        [HttpPost]
        public ActionResult CrearDepartamentos(DepartamentosViewModel model)
        {
            try
            {
                using (var db = new ConsultorioEntities())
                {
                    var departamento = new Departamentos
                    {
                        descripcion = model.descripcion
                    };

                    db.Departamentos.Add(departamento);

                    db.SaveChanges();

                    return Json(new { ok = true }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json(new { ok = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult EditarDepartamentos(int idDepartamento)
        {
            using (var db = new ConsultorioEntities())
            {
                var model = db.Departamentos.Where(x => x.idDepartamento == idDepartamento).
                    Select(x => new DepartamentosViewModel
                    {
                        idDepartamentos = x.idDepartamento,
                        descripcion = x.descripcion
                    }).FirstOrDefault();

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult EditarDepartamentos(DepartamentosViewModel model)
        {
            try
            {
                using (var db = new ConsultorioEntities())
                {
                    var departamento = db.Departamentos.FirstOrDefault(x => x.idDepartamento == model.idDepartamentos);

                    departamento.descripcion = model.descripcion;

                    db.SaveChanges();

                    return Json(new { ok = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { ok = false }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

    }
}