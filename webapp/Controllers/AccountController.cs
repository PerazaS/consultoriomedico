﻿#region Using

using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmartAdminMvc.Models;

#endregion

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        
        // GET: /account/login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            // Store the originating URL so we can attach it to a form field
            var viewModel = new AccountLoginModel { ReturnUrl = returnUrl };

            return View(viewModel);
        }

        // POST: /account/login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AccountLoginModel viewModel)
        {
            
            if (!ModelState.IsValid)
                return View(viewModel);
            
           if(Membership.ValidateUser(viewModel.Email, viewModel.Password))
            {

                FormsAuthentication.SetAuthCookie(viewModel.Email, viewModel.RememberMe);
                if (!string.IsNullOrEmpty(viewModel.ReturnUrl)) return Redirect(viewModel.ReturnUrl);
                return RedirectToAction("Index", "Home");
            }

            return View(viewModel);
        }

        // GET: /account/error
        [AllowAnonymous]
        public ActionResult Error()
        {

            return View();
        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

               

        // GET: /account/lock
        [AllowAnonymous]
        public ActionResult Lock()
        {
            return View();
        }
    }
}