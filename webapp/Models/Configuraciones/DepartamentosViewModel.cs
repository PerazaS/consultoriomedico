﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Configuraciones
{
    public class DepartamentosViewModel
    {
        public int idDepartamentos { get; set; }
        public string descripcion { get; set; }
    }
}